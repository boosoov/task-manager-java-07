package com.rencredit.jschool.boruak.taskmanager;

import com.rencredit.jschool.boruak.taskmanager.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER ** \n");
        parseArgs(args);
        process();
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String[] commands;
        while (true) {
            System.out.print("Enter command: ");
            commands = scanner.nextLine().split("\\s+");
            System.out.println();
            parseArgs(commands);
        }
    }

    private static void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        for (String arg : args) {
            processArg(arg);
            System.out.println();
        }
    }

    private static void processArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.EXIT:
                System.exit((0));
                break;
            default:
                showUnknownCommand(arg);
        }
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Boruak Sergey");
        System.out.println("E-MAIL: boosoov@gmail.com");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConst.ABOUT + " - Show developer info.");
        System.out.println(TerminalConst.VERSION + " - Show version info.");
        System.out.println(TerminalConst.HELP + " - Display terminal command.");
    }

    private static void showUnknownCommand(final String arg) {
        System.out.println("Unknown command: " + arg);
    }

}
